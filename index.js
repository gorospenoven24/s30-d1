const express = require("express");
// require mongoose
const  mongoose = require("mongoose");

const app = express();
const port = 3001;

// connection to mongoDB Atlas
// syntax: mongoose.connect("<Mongodb atlas connection string>,{useNewUrlParser:true}");
mongoose.connect("mongodb+srv://dbnovengorospe:efBaWxyisxGMqFY8@wdc028-course-booking.68ll3.mongodb.net/b138_to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology : true
	}

);


let db = mongoose.connection;

// if a connection error occured, output in the console
// console.error,bind(console) allow us to print errors in the rowser console and in the terminal
db.on("error",console.error.bind(console,"connection error"));

// if the connection is successfull,output in the console
db.once("open", ()=> console.log("were connected to the cloud database")) ;

// schema - determine the structure of the documents to be written in the database
const taskSchema = mongoose.Schema({
	name : String,
	status : {
		type : String,
		default :"pending"
	} 
})

//  create the task model
const Task = mongoose.model("Task",taskSchema);





app.use(express.json());

app.use(express.urlencoded({extended: true}));

// Create a post route to creae a new task
app.post("/tasks",(req,res) =>{
	Task.findOne({name : req.body.name}, (err,result)=>{
		// if a document was found and the documet name matches the information sent via the client / postman
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found");
		}
		else{
			let newTask = new Task({
				name: req.body.name
			});
			newTask.save((saveErr, savedTask)=>{
				if (saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New task created")
				}
			}) 
		}
	})
})

// create a get request to retrive all tasks
app.get("/tasks",(req,res)=>{
	Task.find({},(err,result)=>{
		// if error occured
		if(err){
			return console.log(err);
		}
		// if no error not found
		else{
			return res.status(200).json({
				data:result
			})
		}
	})
})

/////////////////////activyty////////////////////////////////////////


const userSchema = mongoose.Schema({
	username : String,
	password : String
})

const User = mongoose.model("User",userSchema);

app.post("/signup",(req,res) =>{
	Task.findOne({username : req.body.username}, (err,result)=>{
		// if a document was found and the documet name matches the information sent via the client / postman
		if(result != null && result.username == req.body.username){
			return res.send("Duplicate task found");
		}
		else{
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});
			newUser.save((saveErr, savedTask)=>{
				if (saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New User Registered")
				}
			}) 
		}
	})
})







app.listen(port,()=> console.log(`server is running at port ${port}`));